class Polygon: 
    
    #Attributes
    title="rectangle"
    convexity=False
    __area=[(0, 0), (1, 0), (0, 1), (1, 1)]
    _adjacencies=[(1, 2), (1, 3), (3, 4), (2, 4)]
    
    #Constructor
    def __init__(self, dim):
        self.dim=dim
        self.nodes_labels=[i+1 for i in range(dim)]
        
    #Method: A Getter
    def getNeighbours(self, point_x):
        adjacencies_x=[]
        for couple in self._adjacencies:
            if point_x in couple:
                adjacencies_x.append(couple[1-couple.index(point_x)])
        return adjacencies_x
    
    # Method: A Setter
    def cleanNeighbours(self, point_x):
        copy_adjacencies=self._adjacencies[:]
        for couple in self._adjacencies:
            if point_x in couple:
                copy_adjacencies.remove(couple) 
        self._adjacencies=copy_adjacencies[:]
    
    def getArea(self):
        return self.__area
    
    def getAdjacencies(self):
        return self._adjacencies
    
    def setAdjacency(self, t):
        self._adjacencies.append(t)

class Rectangle(Polygon):
    #Constructor
    def __init__(self, dim):
        self.dim=dim
        self.nodes_labels=[i+1 for i in range(dim)]